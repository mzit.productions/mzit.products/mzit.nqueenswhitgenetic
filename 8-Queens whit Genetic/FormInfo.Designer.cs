﻿namespace _8_Queens_whit_Genetic
{
    partial class FormInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInfo));
            this.tlpAboutUs = new System.Windows.Forms.TableLayoutPanel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.tlpANP = new System.Windows.Forms.TableLayoutPanel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.flpProgrammer = new System.Windows.Forms.FlowLayoutPanel();
            this.lblProgrammerName_0 = new System.Windows.Forms.Label();
            this.lblProgrammerName_1 = new System.Windows.Forms.Label();
            this.lblProgrammerName_2 = new System.Windows.Forms.Label();
            this.linkLblEmail = new System.Windows.Forms.LinkLabel();
            this.tlpAboutUs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.tlpANP.SuspendLayout();
            this.flpProgrammer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpAboutUs
            // 
            this.tlpAboutUs.ColumnCount = 3;
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tlpAboutUs.Controls.Add(this.picLogo, 0, 0);
            this.tlpAboutUs.Controls.Add(this.tlpANP, 2, 0);
            this.tlpAboutUs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutUs.Location = new System.Drawing.Point(0, 0);
            this.tlpAboutUs.Name = "tlpAboutUs";
            this.tlpAboutUs.Padding = new System.Windows.Forms.Padding(20);
            this.tlpAboutUs.RowCount = 1;
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpAboutUs.Size = new System.Drawing.Size(524, 281);
            this.tlpAboutUs.TabIndex = 2;
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Image = global::_8_Queens_whit_Genetic.Properties.Resources.logo_x1281;
            this.picLogo.Location = new System.Drawing.Point(23, 23);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(187, 235);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // tlpANP
            // 
            this.tlpANP.ColumnCount = 1;
            this.tlpANP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpANP.Controls.Add(this.lblVersion, 0, 2);
            this.tlpANP.Controls.Add(this.lblProductName, 0, 0);
            this.tlpANP.Controls.Add(this.flpProgrammer, 0, 1);
            this.tlpANP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpANP.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpANP.Location = new System.Drawing.Point(240, 23);
            this.tlpANP.Name = "tlpANP";
            this.tlpANP.RowCount = 3;
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpANP.Size = new System.Drawing.Size(261, 235);
            this.tlpANP.TabIndex = 34;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblVersion.Location = new System.Drawing.Point(3, 202);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(255, 25);
            this.lblVersion.TabIndex = 336;
            this.lblVersion.Text = "1.00.60410";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductName
            // 
            this.lblProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold);
            this.lblProductName.ForeColor = System.Drawing.Color.Red;
            this.lblProductName.Location = new System.Drawing.Point(3, 18);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(255, 23);
            this.lblProductName.TabIndex = 312;
            this.lblProductName.Text = "مسئله هشت وزیر به کمک ژنتیک";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flpProgrammer
            // 
            this.flpProgrammer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpProgrammer.Controls.Add(this.lblProgrammerName_0);
            this.flpProgrammer.Controls.Add(this.lblProgrammerName_1);
            this.flpProgrammer.Controls.Add(this.lblProgrammerName_2);
            this.flpProgrammer.Controls.Add(this.linkLblEmail);
            this.flpProgrammer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpProgrammer.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpProgrammer.Location = new System.Drawing.Point(3, 63);
            this.flpProgrammer.Name = "flpProgrammer";
            this.flpProgrammer.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.flpProgrammer.Size = new System.Drawing.Size(255, 129);
            this.flpProgrammer.TabIndex = 337;
            // 
            // lblProgrammerName_0
            // 
            this.lblProgrammerName_0.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProgrammerName_0.ForeColor = System.Drawing.Color.Blue;
            this.lblProgrammerName_0.Location = new System.Drawing.Point(3, 5);
            this.lblProgrammerName_0.Name = "lblProgrammerName_0";
            this.lblProgrammerName_0.Size = new System.Drawing.Size(255, 25);
            this.lblProgrammerName_0.TabIndex = 336;
            this.lblProgrammerName_0.Text = "HiVE Productions - Mehdi MzIT";
            this.lblProgrammerName_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProgrammerName_1
            // 
            this.lblProgrammerName_1.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProgrammerName_1.ForeColor = System.Drawing.Color.Blue;
            this.lblProgrammerName_1.Location = new System.Drawing.Point(3, 30);
            this.lblProgrammerName_1.Name = "lblProgrammerName_1";
            this.lblProgrammerName_1.Size = new System.Drawing.Size(255, 25);
            this.lblProgrammerName_1.TabIndex = 338;
            this.lblProgrammerName_1.Text = "HiVE Productions - Mehdi MzIT";
            this.lblProgrammerName_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblProgrammerName_1.Visible = false;
            // 
            // lblProgrammerName_2
            // 
            this.lblProgrammerName_2.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProgrammerName_2.ForeColor = System.Drawing.Color.Blue;
            this.lblProgrammerName_2.Location = new System.Drawing.Point(3, 55);
            this.lblProgrammerName_2.Name = "lblProgrammerName_2";
            this.lblProgrammerName_2.Size = new System.Drawing.Size(255, 25);
            this.lblProgrammerName_2.TabIndex = 339;
            this.lblProgrammerName_2.Text = "HiVE Productions - Mehdi MzIT";
            this.lblProgrammerName_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblProgrammerName_2.Visible = false;
            // 
            // linkLblEmail
            // 
            this.linkLblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblEmail.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.linkLblEmail.LinkColor = System.Drawing.Color.DarkMagenta;
            this.linkLblEmail.Location = new System.Drawing.Point(3, 80);
            this.linkLblEmail.Name = "linkLblEmail";
            this.linkLblEmail.Size = new System.Drawing.Size(255, 25);
            this.linkLblEmail.TabIndex = 337;
            this.linkLblEmail.TabStop = true;
            this.linkLblEmail.Text = "PTP.HiVE@gmail.com";
            this.linkLblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(524, 281);
            this.Controls.Add(this.tlpAboutUs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            this.tlpAboutUs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.tlpANP.ResumeLayout(false);
            this.tlpANP.PerformLayout();
            this.flpProgrammer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpAboutUs;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.TableLayoutPanel tlpANP;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.FlowLayoutPanel flpProgrammer;
        private System.Windows.Forms.Label lblProgrammerName_0;
        private System.Windows.Forms.LinkLabel linkLblEmail;
        private System.Windows.Forms.Label lblProgrammerName_1;
        private System.Windows.Forms.Label lblProgrammerName_2;
    }
}