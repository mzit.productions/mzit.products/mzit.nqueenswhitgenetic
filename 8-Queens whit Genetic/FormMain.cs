﻿using _8_Queens_whit_Genetic.Properties;
using ECP.PersianMessageBox;
using System;
using System.Windows.Forms;

namespace _8_Queens_whit_Genetic
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void lblNameProduction_Click(object sender, EventArgs e)
        {
            try
            {
                MPG_WinShutDowm.ShowShutForm _FormInfo = new MPG_WinShutDowm.ShowShutForm(new FormInfo());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //----------

        #region Genetic Algorithm

        private void Fitness(int[,] x)
        {
            int Fit = 0;

            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                Fit = 0;
                for (int j = 0; j < Settings.Default.nQueens; j++)
                {
                    for (int t = 0; t < j; t++)
                    {
                        if (Math.Abs(j - t) == Math.Abs(x[i, j]) - x[i, t])
                        {
                            Fit++;
                        }
                    }
                    for (int t = j + 1; t < Settings.Default.nQueens; t++)
                    {
                        if (x[i, j] == x[i, t])
                        {
                            Fit++;
                        }
                        if (Math.Abs(j - t) == Math.Abs(x[i, j]) - x[i, t])
                        {
                            Fit++;
                        }
                    }
                }
                x[i, Settings.Default.nQueens] = Fit;
            }
        }

        private void Sort(int[,] x)
        {
            int temp;

            for (int i = 0; i < (Settings.Default.NumberChromosomes - 1); i++)
            {
                for (int j = (i + 1); j < Settings.Default.NumberChromosomes; j++)
                {
                    if (x[i, Settings.Default.nQueens] > x[j, Settings.Default.nQueens])
                    {
                        for (int k = 0; k <= Settings.Default.nQueens; k++)
                        {
                            temp = x[i, k];
                            x[i, k] = x[j, k];
                            x[j, k] = temp;
                        }
                    }
                }
            }
        }

        private void CrossOver(int[,] currentGeneration, int[,] newGeneration)
        {
            for (int i = 0; i < (Settings.Default.NumberChromosomes / 2); i++)
            {
                for (int j = 0; j <= Settings.Default.nQueens; j++)
                {
                    newGeneration[i, j] = currentGeneration[i, j];
                }
            }

            for (int i = (Settings.Default.NumberChromosomes / 2); i < Settings.Default.NumberChromosomes - 1; i += 2)
            {
                for (int j = 0; j <= Settings.Default.nQueens; j++)
                {
                    if (j < ((Settings.Default.PercentCrossOver / 100) * Settings.Default.nQueens))
                    {
                        newGeneration[i, j] = currentGeneration[i, j];
                        newGeneration[i + 1, j] = currentGeneration[i + 1, j];
                    }
                    else
                    {
                        newGeneration[i, j] = currentGeneration[i + 1, j];
                        newGeneration[i + 1, j] = currentGeneration[i, j];
                    }
                }
            }
        }

        private void Mutation(int[,] x)
        {
            Random r = new Random();
            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                if (r.Next(0, 100) >= Settings.Default.PercentMutation)
                {
                    x[i, r.Next(0, Settings.Default.nQueens)] = r.Next(1, Settings.Default.nQueens + 1);
                }
            }
        }

        private void Print(int[,] iGeneration, ListBox listboxi)
        {
            listboxi.Items.Clear();
            listboxi.Items.Add("The CrossOver of Chromosomes is:");
            string iChromosome = string.Empty;

            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                int j;
                iChromosome = "";
                for (j = 0; j < Settings.Default.nQueens; j++)
                {
                    iChromosome += " " + iGeneration[i, j];
                }
                iChromosome += "    -->  ";
                iChromosome += iGeneration[i, j];
                listboxi.Items.Add(iChromosome);
            }
        }

        private void CreateBaseChromosomes(int[,] currentGeneration, int[,] newGeneration)
        {
            listBoxBaseChromosomes.Items.Clear();
            listBoxBaseChromosomes.Items.Add("The Base of Chromosomes is:");
            string BaseChromosome = string.Empty; ;

            Random r = new Random();
            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                BaseChromosome = "";
                for (int j = 0; j < Settings.Default.nQueens; j++)
                {
                    newGeneration[i, j] = 2;
                    currentGeneration[i, j] = r.Next(1, Settings.Default.nQueens + 1);
                    BaseChromosome += " " + currentGeneration[i, j];
                }
                listBoxBaseChromosomes.Items.Add(BaseChromosome);
            }

            Fitness(currentGeneration);
            Sort(currentGeneration);

            Fitness(newGeneration);
            Sort(newGeneration);
        }

        private void RunGeneticAlgorithm()
        {
            int[,] currentGeneration = new int[Settings.Default.NumberChromosomes, Settings.Default.nQueens + 1];
            int[,] newGeneration = new int[Settings.Default.NumberChromosomes, Settings.Default.nQueens + 1];

            Settings.Default.nRunGA = 0;
            Settings.Default.ResultValue = 0;
            CreateBaseChromosomes(currentGeneration, newGeneration);

            while ((currentGeneration[0, Settings.Default.nQueens] != 0) && (Settings.Default.nRunGA < Settings.Default.LimitedCrossOver))
            {
                CrossOver(currentGeneration, newGeneration);
                Mutation(newGeneration);
                Fitness(newGeneration);
                Sort(newGeneration);

                Print(newGeneration, listBoxCrossOverChromosomes);

                currentGeneration = newGeneration;
                Settings.Default.nRunGA++;
            }

            if (currentGeneration[0, Settings.Default.nQueens] == 0)
            {
                Settings.Default.ResultValue = 1;
            }

            string iChromosome = "";
            for (int i = 0; i < Settings.Default.nQueens; i++)
            {
                iChromosome += currentGeneration[0, i];
            }
            Settings.Default.ResultFinal = iChromosome;
        }

        #endregion Genetic Algorithm

        //----------

        #region DetailsProblem

        private void LoadDetailsProblem()
        {
            intNumberChromosomes.Value = Settings.Default.NumberChromosomes;
            intLimitedCrossOver.Value = Settings.Default.LimitedCrossOver;
            intPercentCrossOver.Value = Settings.Default.PercentCrossOver;
            intPercentMutation.Value = Settings.Default.PercentMutation;
        }

        private void intNumberChromosomes_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.NumberChromosomes = Convert.ToInt32(intNumberChromosomes.Value);
        }

        private void intLimitedCrossOver_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.LimitedCrossOver = Convert.ToInt32(intLimitedCrossOver.Value);
        }

        private void intPercentCrossOver_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.PercentCrossOver = Convert.ToInt32(intPercentCrossOver.Value);
        }

        private void intPercentMutation_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.PercentMutation = Convert.ToInt32(intPercentMutation.Value);
        }

        private bool ErrorRecordNewGene()
        {
            bool isError = false;

            Settings.Default.ResultFinal = " قسمت‌های زیر را تعیین کنید:        \n";

            if (intNumberChromosomes.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  تعداد کروموزوم‌های اولیه ";
                isError = true;
            }
            if (intLimitedCrossOver.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  حداکثر تعداد تولید فرزندان ";
                isError = true;
            }
            if (intPercentCrossOver.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  درصد ترکیب والدها ";
                isError = true;
            }
            if (intPercentMutation.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  درصد جهش‌ها ";
                isError = true;
            }

            Settings.Default.ResultFinal += "\n\n";

            return isError;
        }

        private void btnResultNewGene_Click(object sender, EventArgs e)
        {
            if (ErrorRecordNewGene() == false)
            {
                if (PersianMessageBox.Show(" آیا می‌خواهید فرآیند‌ جدیدی جستجو کنید؟    ",
                                               "جستجو فرآیند جدید",
                                               PersianMessageBox.Buttons.YesNo,
                                               PersianMessageBox.Icon.Question,
                                               PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                {
                    Cursor = Cursors.WaitCursor;

                    RunGeneticAlgorithm();
                    ResultGene();
                    LoadChessPage();

                    Cursor = Cursors.Default;

                    PersianMessageBox.Show(Settings.Default.MessageRecord,
                                           "جزییات فرآیند جدید",
                                           PersianMessageBox.Buttons.OK,
                                           PersianMessageBox.Icon.Information,
                                           PersianMessageBox.DefaultButton.Button1);
                }
            }

            else
            {
                PersianMessageBox.Show(Settings.Default.ResultFinal,
                                       "خطا",
                                       PersianMessageBox.Buttons.OK,
                                       PersianMessageBox.Icon.Error,
                                       PersianMessageBox.DefaultButton.Button1);
            }
        }

        #endregion DetailsProblem

        //----------

        #region ExecuteFinal

        private void LoadExecuteFinal()
        {
            panelResultValueIcon.BackgroundImage = Resources.isResultValue;
            panelResultValueIcon.Visible = false;
            tblExecuteFinal.Visible = false;
            LoadDetailsProblem();
            lblnRunGA.Text = "";
            lblResultFinal.Text = "";

            int index = 0;

            lblColumn1.Text = string.Format("{0} is?", index++);
            lblColumn2.Text = string.Format("{0} is?", index++);
            lblColumn3.Text = string.Format("{0} is?", index++);
            lblColumn4.Text = string.Format("{0} is?", index++);
            lblColumn5.Text = string.Format("{0} is?", index++);
            lblColumn6.Text = string.Format("{0} is?", index++);
            lblColumn7.Text = string.Format("{0} is?", index++);
            lblColumn8.Text = string.Format("{0} is?", index++);
        }

        private void LoadChessPage()
        {
            int x = Int32.Parse(Settings.Default.ResultFinal);
            int[] rf = new int[Settings.Default.nQueens];

            for (int i = Settings.Default.nQueens; i > 0; i--)
            {
                rf[i - 1] = x % 10;
                x /= 10;
            }

            lblColumn1.Text = rf[0].ToString();
            lblColumn2.Text = rf[1].ToString();
            lblColumn3.Text = rf[2].ToString();
            lblColumn4.Text = rf[3].ToString();
            lblColumn5.Text = rf[4].ToString();
            lblColumn6.Text = rf[5].ToString();
            lblColumn7.Text = rf[6].ToString();
            lblColumn8.Text = rf[7].ToString();
        }

        private void ResultGene()
        {
            if (Settings.Default.ResultValue == 1)
            {
                panelResultValueIcon.BackgroundImage = Resources.ResultValueTrue;

                panelQueen1.BackgroundImage = Resources.isQueen_1;
                panelQueen2.BackgroundImage = Resources.isQueen_1;
                panelQueen3.BackgroundImage = Resources.isQueen_1;
                panelQueen4.BackgroundImage = Resources.isQueen_1;
                panelQueen5.BackgroundImage = Resources.isQueen_1;
                panelQueen6.BackgroundImage = Resources.isQueen_1;
                panelQueen7.BackgroundImage = Resources.isQueen_1;
                panelQueen8.BackgroundImage = Resources.isQueen_1;

                Settings.Default.MessageRecord = " فرآیند‌ جدید جستجو شد.    " +
                                                 "\n\n مسئله در مرحله " + Settings.Default.nRunGA + " به جواب رسید. ";
            }
            else
            {
                panelResultValueIcon.BackgroundImage = Resources.ResultValueFalse;

                panelQueen1.BackgroundImage = Resources.isQueen_0;
                panelQueen2.BackgroundImage = Resources.isQueen_0;
                panelQueen3.BackgroundImage = Resources.isQueen_0;
                panelQueen4.BackgroundImage = Resources.isQueen_0;
                panelQueen5.BackgroundImage = Resources.isQueen_0;
                panelQueen6.BackgroundImage = Resources.isQueen_0;
                panelQueen7.BackgroundImage = Resources.isQueen_0;
                panelQueen8.BackgroundImage = Resources.isQueen_0;

                Settings.Default.MessageRecord = " فرآیند‌ جدید جستجو شد.    " +
                                                 "\n\n مسئله در " + Settings.Default.nRunGA + " مرحله به جواب نرسید. ";
            }

            tblExecuteFinal.Visible = true;
            panelResultValueIcon.Visible = true;

            lblnRunGA.Text = Settings.Default.nRunGA.ToString();
            lblResultFinal.Text = Settings.Default.ResultFinal;
        }

        #endregion ExecuteFinal

        //------------
        ////----------////---------------------------------------------------------------------// Begin Form_Load
        //------------

        private void FormMain_Load(object sender, EventArgs e)
        {
            try
            {
                MPG_WinShutDowm.ShowShutForm _FormInfo = new MPG_WinShutDowm.ShowShutForm(new FormInfo());

                LoadExecuteFinal();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// End Form_Load
        //------------
    }
}
