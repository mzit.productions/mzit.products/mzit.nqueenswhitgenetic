﻿namespace _8_Queens_whit_Genetic
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tblResults = new System.Windows.Forms.TableLayoutPanel();
            this.listBoxCrossOverChromosomes = new System.Windows.Forms.ListBox();
            this.listBoxBaseChromosomes = new System.Windows.Forms.ListBox();
            this.flpSetting = new System.Windows.Forms.FlowLayoutPanel();
            this.grbNumberChromosomes = new System.Windows.Forms.GroupBox();
            this.intNumberChromosomes = new System.Windows.Forms.NumericUpDown();
            this.grbLimitedCrossOver = new System.Windows.Forms.GroupBox();
            this.intLimitedCrossOver = new System.Windows.Forms.NumericUpDown();
            this.grbPercentCrossOver = new System.Windows.Forms.GroupBox();
            this.intPercentCrossOver = new System.Windows.Forms.NumericUpDown();
            this.lblPercentCrossOver = new System.Windows.Forms.Label();
            this.grbPercentMutation = new System.Windows.Forms.GroupBox();
            this.intPercentMutation = new System.Windows.Forms.NumericUpDown();
            this.lblPercentMutation = new System.Windows.Forms.Label();
            this.btnResultNewGene = new System.Windows.Forms.Button();
            this.panelResult = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flpResult = new System.Windows.Forms.FlowLayoutPanel();
            this.tblExecuteFinal = new System.Windows.Forms.TableLayoutPanel();
            this.lblResultFinal = new System.Windows.Forms.Label();
            this.panelResultValue = new System.Windows.Forms.Panel();
            this.lblnRunGA = new System.Windows.Forms.Label();
            this.panelResultValueIcon = new System.Windows.Forms.Panel();
            this.tlpColumnsOfQueens = new System.Windows.Forms.TableLayoutPanel();
            this.panelQueen8 = new System.Windows.Forms.Panel();
            this.panelQueen5 = new System.Windows.Forms.Panel();
            this.panelQueen2 = new System.Windows.Forms.Panel();
            this.lblColumn3 = new System.Windows.Forms.Label();
            this.lblColumn2 = new System.Windows.Forms.Label();
            this.lblColumn1 = new System.Windows.Forms.Label();
            this.lblColumn8 = new System.Windows.Forms.Label();
            this.lblColumn7 = new System.Windows.Forms.Label();
            this.lblColumn6 = new System.Windows.Forms.Label();
            this.lblColumn5 = new System.Windows.Forms.Label();
            this.lblColumn4 = new System.Windows.Forms.Label();
            this.panelQueen1 = new System.Windows.Forms.Panel();
            this.panelQueen3 = new System.Windows.Forms.Panel();
            this.panelQueen6 = new System.Windows.Forms.Panel();
            this.panelQueen4 = new System.Windows.Forms.Panel();
            this.panelQueen7 = new System.Windows.Forms.Panel();
            this.panelNameProduction = new System.Windows.Forms.Panel();
            this.lblNameProduction = new System.Windows.Forms.Label();
            this.tblResults.SuspendLayout();
            this.flpSetting.SuspendLayout();
            this.grbNumberChromosomes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intNumberChromosomes)).BeginInit();
            this.grbLimitedCrossOver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intLimitedCrossOver)).BeginInit();
            this.grbPercentCrossOver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentCrossOver)).BeginInit();
            this.grbPercentMutation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentMutation)).BeginInit();
            this.panelResult.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flpResult.SuspendLayout();
            this.tblExecuteFinal.SuspendLayout();
            this.panelResultValue.SuspendLayout();
            this.tlpColumnsOfQueens.SuspendLayout();
            this.panelNameProduction.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblResults
            // 
            this.tblResults.BackColor = System.Drawing.SystemColors.Menu;
            this.tblResults.ColumnCount = 3;
            this.tblResults.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tblResults.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
            this.tblResults.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tblResults.Controls.Add(this.listBoxCrossOverChromosomes, 1, 0);
            this.tblResults.Controls.Add(this.listBoxBaseChromosomes, 2, 0);
            this.tblResults.Controls.Add(this.flpSetting, 0, 0);
            this.tblResults.Controls.Add(this.panelResult, 1, 1);
            this.tblResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblResults.Location = new System.Drawing.Point(0, 0);
            this.tblResults.Margin = new System.Windows.Forms.Padding(0);
            this.tblResults.Name = "tblResults";
            this.tblResults.Padding = new System.Windows.Forms.Padding(20);
            this.tblResults.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tblResults.RowCount = 2;
            this.tblResults.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblResults.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tblResults.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tblResults.Size = new System.Drawing.Size(704, 561);
            this.tblResults.TabIndex = 0;
            // 
            // listBoxCrossOverChromosomes
            // 
            this.listBoxCrossOverChromosomes.BackColor = System.Drawing.SystemColors.InfoText;
            this.listBoxCrossOverChromosomes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxCrossOverChromosomes.ForeColor = System.Drawing.Color.YellowGreen;
            this.listBoxCrossOverChromosomes.FormattingEnabled = true;
            this.listBoxCrossOverChromosomes.Location = new System.Drawing.Point(236, 23);
            this.listBoxCrossOverChromosomes.Name = "listBoxCrossOverChromosomes";
            this.listBoxCrossOverChromosomes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.listBoxCrossOverChromosomes.Size = new System.Drawing.Size(233, 424);
            this.listBoxCrossOverChromosomes.TabIndex = 6;
            // 
            // listBoxBaseChromosomes
            // 
            this.listBoxBaseChromosomes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxBaseChromosomes.FormattingEnabled = true;
            this.listBoxBaseChromosomes.Location = new System.Drawing.Point(23, 23);
            this.listBoxBaseChromosomes.Name = "listBoxBaseChromosomes";
            this.listBoxBaseChromosomes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.listBoxBaseChromosomes.Size = new System.Drawing.Size(207, 424);
            this.listBoxBaseChromosomes.TabIndex = 3;
            // 
            // flpSetting
            // 
            this.flpSetting.BackColor = System.Drawing.Color.White;
            this.flpSetting.Controls.Add(this.grbNumberChromosomes);
            this.flpSetting.Controls.Add(this.grbLimitedCrossOver);
            this.flpSetting.Controls.Add(this.grbPercentCrossOver);
            this.flpSetting.Controls.Add(this.grbPercentMutation);
            this.flpSetting.Controls.Add(this.btnResultNewGene);
            this.flpSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpSetting.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpSetting.Location = new System.Drawing.Point(475, 23);
            this.flpSetting.Name = "flpSetting";
            this.flpSetting.Padding = new System.Windows.Forms.Padding(10, 20, 0, 0);
            this.flpSetting.Size = new System.Drawing.Size(206, 424);
            this.flpSetting.TabIndex = 1;
            // 
            // grbNumberChromosomes
            // 
            this.grbNumberChromosomes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.grbNumberChromosomes.Controls.Add(this.intNumberChromosomes);
            this.grbNumberChromosomes.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbNumberChromosomes.Location = new System.Drawing.Point(17, 23);
            this.grbNumberChromosomes.Name = "grbNumberChromosomes";
            this.grbNumberChromosomes.Size = new System.Drawing.Size(176, 65);
            this.grbNumberChromosomes.TabIndex = 3;
            this.grbNumberChromosomes.TabStop = false;
            this.grbNumberChromosomes.Text = "تعداد کروموزوم‌های اولیه";
            // 
            // intNumberChromosomes
            // 
            this.intNumberChromosomes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.intNumberChromosomes.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.intNumberChromosomes.Location = new System.Drawing.Point(6, 30);
            this.intNumberChromosomes.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.intNumberChromosomes.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.intNumberChromosomes.Name = "intNumberChromosomes";
            this.intNumberChromosomes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.intNumberChromosomes.Size = new System.Drawing.Size(164, 29);
            this.intNumberChromosomes.TabIndex = 0;
            this.intNumberChromosomes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.intNumberChromosomes.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.intNumberChromosomes.ValueChanged += new System.EventHandler(this.intNumberChromosomes_ValueChanged);
            // 
            // grbLimitedCrossOver
            // 
            this.grbLimitedCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.grbLimitedCrossOver.Controls.Add(this.intLimitedCrossOver);
            this.grbLimitedCrossOver.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbLimitedCrossOver.Location = new System.Drawing.Point(17, 94);
            this.grbLimitedCrossOver.Name = "grbLimitedCrossOver";
            this.grbLimitedCrossOver.Size = new System.Drawing.Size(176, 65);
            this.grbLimitedCrossOver.TabIndex = 5;
            this.grbLimitedCrossOver.TabStop = false;
            this.grbLimitedCrossOver.Text = "حداکثر تعداد تولید فرزندان";
            // 
            // intLimitedCrossOver
            // 
            this.intLimitedCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.intLimitedCrossOver.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.intLimitedCrossOver.Location = new System.Drawing.Point(6, 30);
            this.intLimitedCrossOver.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.intLimitedCrossOver.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.intLimitedCrossOver.Name = "intLimitedCrossOver";
            this.intLimitedCrossOver.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.intLimitedCrossOver.Size = new System.Drawing.Size(164, 29);
            this.intLimitedCrossOver.TabIndex = 1;
            this.intLimitedCrossOver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.intLimitedCrossOver.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.intLimitedCrossOver.ValueChanged += new System.EventHandler(this.intLimitedCrossOver_ValueChanged);
            // 
            // grbPercentCrossOver
            // 
            this.grbPercentCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.grbPercentCrossOver.Controls.Add(this.intPercentCrossOver);
            this.grbPercentCrossOver.Controls.Add(this.lblPercentCrossOver);
            this.grbPercentCrossOver.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbPercentCrossOver.Location = new System.Drawing.Point(17, 165);
            this.grbPercentCrossOver.Name = "grbPercentCrossOver";
            this.grbPercentCrossOver.Size = new System.Drawing.Size(176, 65);
            this.grbPercentCrossOver.TabIndex = 11;
            this.grbPercentCrossOver.TabStop = false;
            this.grbPercentCrossOver.Text = "درصد ترکیب والدها";
            // 
            // intPercentCrossOver
            // 
            this.intPercentCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.intPercentCrossOver.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.intPercentCrossOver.Location = new System.Drawing.Point(24, 30);
            this.intPercentCrossOver.Name = "intPercentCrossOver";
            this.intPercentCrossOver.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.intPercentCrossOver.Size = new System.Drawing.Size(146, 29);
            this.intPercentCrossOver.TabIndex = 5;
            this.intPercentCrossOver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.intPercentCrossOver.ValueChanged += new System.EventHandler(this.intPercentCrossOver_ValueChanged);
            // 
            // lblPercentCrossOver
            // 
            this.lblPercentCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPercentCrossOver.AutoSize = true;
            this.lblPercentCrossOver.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblPercentCrossOver.Location = new System.Drawing.Point(2, 30);
            this.lblPercentCrossOver.Name = "lblPercentCrossOver";
            this.lblPercentCrossOver.Size = new System.Drawing.Size(25, 25);
            this.lblPercentCrossOver.TabIndex = 4;
            this.lblPercentCrossOver.Text = "%";
            this.lblPercentCrossOver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grbPercentMutation
            // 
            this.grbPercentMutation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.grbPercentMutation.Controls.Add(this.intPercentMutation);
            this.grbPercentMutation.Controls.Add(this.lblPercentMutation);
            this.grbPercentMutation.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbPercentMutation.Location = new System.Drawing.Point(17, 236);
            this.grbPercentMutation.Name = "grbPercentMutation";
            this.grbPercentMutation.Size = new System.Drawing.Size(176, 59);
            this.grbPercentMutation.TabIndex = 12;
            this.grbPercentMutation.TabStop = false;
            this.grbPercentMutation.Text = "درصد جهش‌ها";
            // 
            // intPercentMutation
            // 
            this.intPercentMutation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.intPercentMutation.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.intPercentMutation.Location = new System.Drawing.Point(24, 24);
            this.intPercentMutation.Name = "intPercentMutation";
            this.intPercentMutation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.intPercentMutation.Size = new System.Drawing.Size(146, 29);
            this.intPercentMutation.TabIndex = 6;
            this.intPercentMutation.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.intPercentMutation.ValueChanged += new System.EventHandler(this.intPercentMutation_ValueChanged);
            // 
            // lblPercentMutation
            // 
            this.lblPercentMutation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPercentMutation.AutoSize = true;
            this.lblPercentMutation.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblPercentMutation.Location = new System.Drawing.Point(2, 24);
            this.lblPercentMutation.Name = "lblPercentMutation";
            this.lblPercentMutation.Size = new System.Drawing.Size(25, 25);
            this.lblPercentMutation.TabIndex = 5;
            this.lblPercentMutation.Text = "%";
            this.lblPercentMutation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnResultNewGene
            // 
            this.btnResultNewGene.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResultNewGene.AutoSize = true;
            this.btnResultNewGene.BackColor = System.Drawing.Color.Black;
            this.btnResultNewGene.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnResultNewGene.Font = new System.Drawing.Font("B Titr", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnResultNewGene.ForeColor = System.Drawing.Color.SeaShell;
            this.btnResultNewGene.Location = new System.Drawing.Point(17, 338);
            this.btnResultNewGene.Margin = new System.Windows.Forms.Padding(3, 40, 3, 3);
            this.btnResultNewGene.Name = "btnResultNewGene";
            this.btnResultNewGene.Size = new System.Drawing.Size(176, 41);
            this.btnResultNewGene.TabIndex = 13;
            this.btnResultNewGene.Text = "جستجو ...";
            this.btnResultNewGene.UseVisualStyleBackColor = false;
            this.btnResultNewGene.Click += new System.EventHandler(this.btnResultNewGene_Click);
            // 
            // panelResult
            // 
            this.tblResults.SetColumnSpan(this.panelResult, 3);
            this.panelResult.Controls.Add(this.tableLayoutPanel2);
            this.panelResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResult.Location = new System.Drawing.Point(23, 459);
            this.panelResult.Name = "panelResult";
            this.panelResult.Size = new System.Drawing.Size(658, 79);
            this.panelResult.TabIndex = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68F));
            this.tableLayoutPanel2.Controls.Add(this.flpResult, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panelNameProduction, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(658, 79);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // flpResult
            // 
            this.flpResult.Controls.Add(this.tblExecuteFinal);
            this.flpResult.Controls.Add(this.tlpColumnsOfQueens);
            this.flpResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpResult.Location = new System.Drawing.Point(0, 0);
            this.flpResult.Margin = new System.Windows.Forms.Padding(0);
            this.flpResult.Name = "flpResult";
            this.flpResult.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flpResult.Size = new System.Drawing.Size(448, 79);
            this.flpResult.TabIndex = 5;
            // 
            // tblExecuteFinal
            // 
            this.tblExecuteFinal.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tblExecuteFinal.ColumnCount = 1;
            this.tblExecuteFinal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblExecuteFinal.Controls.Add(this.lblResultFinal, 0, 1);
            this.tblExecuteFinal.Controls.Add(this.panelResultValue, 0, 0);
            this.tblExecuteFinal.Location = new System.Drawing.Point(0, 3);
            this.tblExecuteFinal.Margin = new System.Windows.Forms.Padding(0);
            this.tblExecuteFinal.Name = "tblExecuteFinal";
            this.tblExecuteFinal.RowCount = 2;
            this.tblExecuteFinal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tblExecuteFinal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tblExecuteFinal.Size = new System.Drawing.Size(85, 66);
            this.tblExecuteFinal.TabIndex = 19;
            // 
            // lblResultFinal
            // 
            this.lblResultFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResultFinal.AutoSize = true;
            this.lblResultFinal.Location = new System.Drawing.Point(3, 47);
            this.lblResultFinal.Name = "lblResultFinal";
            this.lblResultFinal.Size = new System.Drawing.Size(79, 13);
            this.lblResultFinal.TabIndex = 17;
            this.lblResultFinal.Text = "-";
            this.lblResultFinal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelResultValue
            // 
            this.panelResultValue.Controls.Add(this.lblnRunGA);
            this.panelResultValue.Controls.Add(this.panelResultValueIcon);
            this.panelResultValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResultValue.Location = new System.Drawing.Point(3, 3);
            this.panelResultValue.Name = "panelResultValue";
            this.panelResultValue.Size = new System.Drawing.Size(79, 30);
            this.panelResultValue.TabIndex = 16;
            // 
            // lblnRunGA
            // 
            this.lblnRunGA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblnRunGA.AutoSize = true;
            this.lblnRunGA.Location = new System.Drawing.Point(36, 6);
            this.lblnRunGA.Name = "lblnRunGA";
            this.lblnRunGA.Size = new System.Drawing.Size(10, 13);
            this.lblnRunGA.TabIndex = 15;
            this.lblnRunGA.Text = "-";
            this.lblnRunGA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelResultValueIcon
            // 
            this.panelResultValueIcon.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelResultValueIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelResultValueIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelResultValueIcon.Location = new System.Drawing.Point(0, 0);
            this.panelResultValueIcon.Name = "panelResultValueIcon";
            this.panelResultValueIcon.Size = new System.Drawing.Size(29, 30);
            this.panelResultValueIcon.TabIndex = 14;
            // 
            // tlpColumnsOfQueens
            // 
            this.tlpColumnsOfQueens.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tlpColumnsOfQueens.ColumnCount = 8;
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen8, 7, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen5, 4, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen2, 1, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn3, 2, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn2, 1, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn1, 0, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn8, 7, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn7, 6, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn6, 5, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn5, 4, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.lblColumn4, 3, 1);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen1, 0, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen3, 2, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen6, 5, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen4, 3, 0);
            this.tlpColumnsOfQueens.Controls.Add(this.panelQueen7, 6, 0);
            this.tlpColumnsOfQueens.Location = new System.Drawing.Point(85, 0);
            this.tlpColumnsOfQueens.Margin = new System.Windows.Forms.Padding(0);
            this.tlpColumnsOfQueens.Name = "tlpColumnsOfQueens";
            this.tlpColumnsOfQueens.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tlpColumnsOfQueens.RowCount = 2;
            this.tlpColumnsOfQueens.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpColumnsOfQueens.Size = new System.Drawing.Size(288, 72);
            this.tlpColumnsOfQueens.TabIndex = 20;
            // 
            // panelQueen8
            // 
            this.panelQueen8.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen8.Location = new System.Drawing.Point(255, 3);
            this.panelQueen8.Name = "panelQueen8";
            this.panelQueen8.Size = new System.Drawing.Size(30, 30);
            this.panelQueen8.TabIndex = 12;
            // 
            // panelQueen5
            // 
            this.panelQueen5.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen5.Location = new System.Drawing.Point(147, 3);
            this.panelQueen5.Name = "panelQueen5";
            this.panelQueen5.Size = new System.Drawing.Size(30, 30);
            this.panelQueen5.TabIndex = 12;
            // 
            // panelQueen2
            // 
            this.panelQueen2.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen2.Location = new System.Drawing.Point(39, 3);
            this.panelQueen2.Name = "panelQueen2";
            this.panelQueen2.Size = new System.Drawing.Size(30, 30);
            this.panelQueen2.TabIndex = 11;
            // 
            // lblColumn3
            // 
            this.lblColumn3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn3.AutoSize = true;
            this.lblColumn3.Location = new System.Drawing.Point(75, 47);
            this.lblColumn3.Name = "lblColumn3";
            this.lblColumn3.Size = new System.Drawing.Size(30, 13);
            this.lblColumn3.TabIndex = 4;
            this.lblColumn3.Text = "3 is?";
            this.lblColumn3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn2
            // 
            this.lblColumn2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn2.AutoSize = true;
            this.lblColumn2.Location = new System.Drawing.Point(39, 47);
            this.lblColumn2.Name = "lblColumn2";
            this.lblColumn2.Size = new System.Drawing.Size(30, 13);
            this.lblColumn2.TabIndex = 3;
            this.lblColumn2.Text = "2 is?";
            this.lblColumn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn1
            // 
            this.lblColumn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn1.AutoSize = true;
            this.lblColumn1.Location = new System.Drawing.Point(3, 47);
            this.lblColumn1.Name = "lblColumn1";
            this.lblColumn1.Size = new System.Drawing.Size(30, 13);
            this.lblColumn1.TabIndex = 1;
            this.lblColumn1.Text = "1 is?";
            this.lblColumn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn8
            // 
            this.lblColumn8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn8.AutoSize = true;
            this.lblColumn8.Location = new System.Drawing.Point(255, 47);
            this.lblColumn8.Name = "lblColumn8";
            this.lblColumn8.Size = new System.Drawing.Size(30, 13);
            this.lblColumn8.TabIndex = 9;
            this.lblColumn8.Text = "8 is?";
            this.lblColumn8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn7
            // 
            this.lblColumn7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn7.AutoSize = true;
            this.lblColumn7.Location = new System.Drawing.Point(219, 47);
            this.lblColumn7.Name = "lblColumn7";
            this.lblColumn7.Size = new System.Drawing.Size(30, 13);
            this.lblColumn7.TabIndex = 8;
            this.lblColumn7.Text = "7 is?";
            this.lblColumn7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn6
            // 
            this.lblColumn6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn6.AutoSize = true;
            this.lblColumn6.Location = new System.Drawing.Point(183, 47);
            this.lblColumn6.Name = "lblColumn6";
            this.lblColumn6.Size = new System.Drawing.Size(30, 13);
            this.lblColumn6.TabIndex = 7;
            this.lblColumn6.Text = "6 is?";
            this.lblColumn6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn5
            // 
            this.lblColumn5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn5.AutoSize = true;
            this.lblColumn5.Location = new System.Drawing.Point(147, 47);
            this.lblColumn5.Name = "lblColumn5";
            this.lblColumn5.Size = new System.Drawing.Size(30, 13);
            this.lblColumn5.TabIndex = 6;
            this.lblColumn5.Text = "5 is?";
            this.lblColumn5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn4
            // 
            this.lblColumn4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn4.AutoSize = true;
            this.lblColumn4.Location = new System.Drawing.Point(111, 47);
            this.lblColumn4.Name = "lblColumn4";
            this.lblColumn4.Size = new System.Drawing.Size(30, 13);
            this.lblColumn4.TabIndex = 5;
            this.lblColumn4.Text = "4 is?";
            this.lblColumn4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelQueen1
            // 
            this.panelQueen1.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen1.Location = new System.Drawing.Point(3, 3);
            this.panelQueen1.Name = "panelQueen1";
            this.panelQueen1.Size = new System.Drawing.Size(30, 30);
            this.panelQueen1.TabIndex = 10;
            // 
            // panelQueen3
            // 
            this.panelQueen3.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen3.Location = new System.Drawing.Point(75, 3);
            this.panelQueen3.Name = "panelQueen3";
            this.panelQueen3.Size = new System.Drawing.Size(30, 30);
            this.panelQueen3.TabIndex = 11;
            // 
            // panelQueen6
            // 
            this.panelQueen6.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen6.Location = new System.Drawing.Point(183, 3);
            this.panelQueen6.Name = "panelQueen6";
            this.panelQueen6.Size = new System.Drawing.Size(30, 30);
            this.panelQueen6.TabIndex = 12;
            // 
            // panelQueen4
            // 
            this.panelQueen4.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen4.Location = new System.Drawing.Point(111, 3);
            this.panelQueen4.Name = "panelQueen4";
            this.panelQueen4.Size = new System.Drawing.Size(30, 30);
            this.panelQueen4.TabIndex = 12;
            // 
            // panelQueen7
            // 
            this.panelQueen7.BackgroundImage = global::_8_Queens_whit_Genetic.Properties.Resources.isQueen;
            this.panelQueen7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen7.Location = new System.Drawing.Point(219, 3);
            this.panelQueen7.Name = "panelQueen7";
            this.panelQueen7.Size = new System.Drawing.Size(30, 30);
            this.panelQueen7.TabIndex = 13;
            // 
            // panelNameProduction
            // 
            this.panelNameProduction.Controls.Add(this.lblNameProduction);
            this.panelNameProduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelNameProduction.Location = new System.Drawing.Point(451, 3);
            this.panelNameProduction.Name = "panelNameProduction";
            this.panelNameProduction.Size = new System.Drawing.Size(204, 73);
            this.panelNameProduction.TabIndex = 4;
            // 
            // lblNameProduction
            // 
            this.lblNameProduction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblNameProduction.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblNameProduction.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblNameProduction.ForeColor = System.Drawing.Color.Magenta;
            this.lblNameProduction.Location = new System.Drawing.Point(-1, 0);
            this.lblNameProduction.Name = "lblNameProduction";
            this.lblNameProduction.Size = new System.Drawing.Size(205, 73);
            this.lblNameProduction.TabIndex = 20;
            this.lblNameProduction.Text = "مسئله هشت وزیر به کمک ژنتیک";
            this.lblNameProduction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNameProduction.Click += new System.EventHandler(this.lblNameProduction_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(704, 561);
            this.Controls.Add(this.tblResults);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(630, 540);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "8-Queens whit Genetic";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.tblResults.ResumeLayout(false);
            this.flpSetting.ResumeLayout(false);
            this.flpSetting.PerformLayout();
            this.grbNumberChromosomes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.intNumberChromosomes)).EndInit();
            this.grbLimitedCrossOver.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.intLimitedCrossOver)).EndInit();
            this.grbPercentCrossOver.ResumeLayout(false);
            this.grbPercentCrossOver.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentCrossOver)).EndInit();
            this.grbPercentMutation.ResumeLayout(false);
            this.grbPercentMutation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentMutation)).EndInit();
            this.panelResult.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flpResult.ResumeLayout(false);
            this.tblExecuteFinal.ResumeLayout(false);
            this.tblExecuteFinal.PerformLayout();
            this.panelResultValue.ResumeLayout(false);
            this.panelResultValue.PerformLayout();
            this.tlpColumnsOfQueens.ResumeLayout(false);
            this.tlpColumnsOfQueens.PerformLayout();
            this.panelNameProduction.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tblResults;
        private System.Windows.Forms.FlowLayoutPanel flpSetting;
        private System.Windows.Forms.GroupBox grbNumberChromosomes;
        private System.Windows.Forms.NumericUpDown intNumberChromosomes;
        private System.Windows.Forms.GroupBox grbLimitedCrossOver;
        private System.Windows.Forms.NumericUpDown intLimitedCrossOver;
        private System.Windows.Forms.GroupBox grbPercentCrossOver;
        private System.Windows.Forms.NumericUpDown intPercentCrossOver;
        private System.Windows.Forms.Label lblPercentCrossOver;
        private System.Windows.Forms.GroupBox grbPercentMutation;
        private System.Windows.Forms.NumericUpDown intPercentMutation;
        private System.Windows.Forms.Label lblPercentMutation;
        private System.Windows.Forms.Button btnResultNewGene;
        private System.Windows.Forms.ListBox listBoxBaseChromosomes;
        private System.Windows.Forms.ListBox listBoxCrossOverChromosomes;
        private System.Windows.Forms.Panel panelResult;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panelNameProduction;
        private System.Windows.Forms.FlowLayoutPanel flpResult;
        private System.Windows.Forms.TableLayoutPanel tblExecuteFinal;
        private System.Windows.Forms.Label lblResultFinal;
        private System.Windows.Forms.Panel panelResultValue;
        private System.Windows.Forms.Label lblnRunGA;
        private System.Windows.Forms.Panel panelResultValueIcon;
        private System.Windows.Forms.TableLayoutPanel tlpColumnsOfQueens;
        private System.Windows.Forms.Panel panelQueen8;
        private System.Windows.Forms.Panel panelQueen5;
        private System.Windows.Forms.Panel panelQueen2;
        private System.Windows.Forms.Label lblColumn3;
        private System.Windows.Forms.Label lblColumn2;
        private System.Windows.Forms.Label lblColumn1;
        private System.Windows.Forms.Label lblColumn8;
        private System.Windows.Forms.Label lblColumn7;
        private System.Windows.Forms.Label lblColumn6;
        private System.Windows.Forms.Label lblColumn5;
        private System.Windows.Forms.Label lblColumn4;
        private System.Windows.Forms.Panel panelQueen1;
        private System.Windows.Forms.Panel panelQueen3;
        private System.Windows.Forms.Panel panelQueen6;
        private System.Windows.Forms.Panel panelQueen4;
        private System.Windows.Forms.Panel panelQueen7;
        private System.Windows.Forms.Label lblNameProduction;
    }
}

